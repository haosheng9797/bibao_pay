<?php

namespace app\common\validate;

use think\Validate;

class PayValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'userId|充值用戶' => 'require|token',
        'money|充值金額' => 'require|number|between:150,2000',
        'payType|充值方式' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'money.require' => '請輸入充值金額',
        'money.number' => '充值金額不可有小數點',
        'money.between' => '充值金額單筆最小150元，最大2000元',
        'payType.require' => '請選擇充值方式',
    ];
}
