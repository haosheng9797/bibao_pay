<?php

namespace app\common\controller;

use think\Controller;
use think\Request;

class Des extends Controller
{
    static function encrypt($data, $key)
    {
        $result = openssl_encrypt($data, "DES-CBC", $key, OPENSSL_RAW_DATA, $key);
        return strtoupper(bin2hex($result));
    }

    static function decrypt($data, $key)
    {
        $data = hex2bin(strtolower($data));
        $result = openssl_decrypt($data, "DES-CBC", $key, OPENSSL_RAW_DATA, $key);
        return $result;
    }
}
