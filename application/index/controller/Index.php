<?php

namespace app\index\controller;


use app\common\controller\Des;
use app\common\validate\PayValidate;
use GuzzleHttp\Client;
use think\Controller;
use think\facade\Session;

class Index extends Controller
{
    public function index()
    {
        return view();
    }

    protected $MerCode = 'sheng0211';
    protected $DESKey = '8Y2gw1Mh';
    protected $listKey = ['8Y2gw1', 'w1Mh'];
    protected $key = 'Kh5deHjdgA';
    protected $host = 'http://adoutox.gosafepp.com';

    public function pay()
    {
        $params = input('post.');
        $validate = new PayValidate();
        if ($validate->check($params) == false) {
            return $this->error_model($validate->getError());
        }
        $url = $this->host . '/api/' . $this->MerCode . '/coin/Login';
        $rate = $params['payType'] != 'bankcard' ? 0.991 : 0.996;
        $data = [
            'UserName' => $params['userId'],
            'Type' => '1',
            'Coin' => 'DC',
            'Amount' => ($params['money'] * $rate),
            'OrderNum' => date('YmdHis') . rand(10000, 99999),
            'PayMethods' => $params['payType']
        ];
        $query = $this->sign($data, ['UserName', 'Type', 'OrderNum']);
        $client = (new Client())->get($url, ['query' => $query]);
        $data = json_decode($client->getBody(), true);
        if ($data['Success'] != 'true') return $this->error_model($data['Message']);
        $data = $data['Data'];
        $url = $data['Url'] . '/' . $data['Token'];
        return redirect($url);
    }

    private function sign($params, $hasParams = [])
    {
        $data = [
            'MerCode' => $this->MerCode,
            'Timestamp' => time() . '100',
        ];
        $data = array_merge($data, $params);
        return [
            'param' => $this->params_hash($data),
            'key' => $this->key($data, $hasParams),
        ];
    }

    public function params_hash($data)
    {
        ksort($data);
        $params_str = '';
        foreach ($data as $key => $val) {
            $params_str .= $key . '=' . $val . '&';
        }
        $params_str = rtrim($params_str, '&');
        $params_hash = Des::encrypt($params_str, $this->DESKey);
        return $params_hash;
    }

    public function key($data, $hasParams = [])
    {
        unset($data['Timestamp']);
        $data_str = $data['MerCode'];
        foreach ($hasParams as $val) {
            $data_str .= $data[$val];
        }
        $data = $data_str . $this->key . date('Ymd');
        $data = md5($data);
        $data = $this->listKey[0] . $data . $this->listKey[1];
        return $data;
    }

    public function error_model($msg = '', $url = null)
    {
        $msg = empty($msg) ? '操作失敗' : $msg;
        Session::flash('error', $msg);
        $url = is_null($url) ? 'window.history.back()' : 'window.location.href="' . $url . '"';
        return '<script>' . $url . '</script>';
    }
}
